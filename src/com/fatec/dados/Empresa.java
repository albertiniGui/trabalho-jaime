package com.fatec.dados;

import java.io.Serializable;
import java.util.List;

public class Empresa implements Serializable {

    private final static long serialVersionUID = 1; // See Nick's comment below

    private String cpnj;
    private String razaoSocial;
    private List<Setor> setorList;

    public Empresa(String razaoSocial, String cpnj, List<Setor> setorList) {
        this.razaoSocial = razaoSocial;
        this.cpnj = cpnj;
        this.setorList = setorList;
    }

    public List<Setor> getSetorList() {
        return setorList;
    }

    public void setSetorList(List<Setor> setorList) {
        this.setorList = setorList;
    }

    @Override
    public String toString() {
        return "Empresa { " +
                "razaoSocial='" + razaoSocial + '\'' +
                ", cpnj='" + cpnj + '\'' +
                ", setores=" + setorList +
                "}";
    }
}
