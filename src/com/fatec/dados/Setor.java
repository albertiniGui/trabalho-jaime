package com.fatec.dados;

import java.io.Serializable;
import java.util.List;

public class Setor implements Serializable {

    private final static long serialVersionUID = 1; // See Nick's comment below

    private int idSetor;
    private String nomeSetor;
    private List<Funcionario> funcionarioList;

    public Setor(){};

    public Setor(int idSetor, String nomeSetor, List<Funcionario> funcionarioList) {
        this.idSetor = idSetor;
        this.nomeSetor = nomeSetor;
        this.funcionarioList = funcionarioList;
    }

    public int getIdSetor() {
        return idSetor;
    }

    public void setNomeSetor(String nomeSetor) {
        this.nomeSetor = nomeSetor;
    }

    public List<Funcionario> getFuncionarioList() {
        return funcionarioList;
    }

    public void setFuncionarioList(List<Funcionario> funcionarioList) {
        this.funcionarioList = funcionarioList;
    }

    @Override
    public String toString() {
        return "\n\tSetor { " +
                "idSetor=" + idSetor +
                ", nomeSetor='" + nomeSetor + '\'' +
                ", funcionarios=" + funcionarioList +
                "\t\n}";
    }
}
